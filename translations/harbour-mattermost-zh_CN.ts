<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <source>This is unofficial client for</source>
        <translation>这是一个非官方客户端</translation>
    </message>
    <message>
        <source>server.</source>
        <translation>服务器</translation>
    </message>
    <message>
        <source>Thanks to</source>
        <translation>感谢</translation>
    </message>
    <message>
        <source>Russian SailfishOS Community channel</source>
        <translation>俄罗斯旗鱼系统社区频道</translation>
    </message>
    <message>
        <source>Sources: </source>
        <translation>源：</translation>
    </message>
    <message>
        <source>If you want to donate, you can do that by:</source>
        <translation>如果你想捐赠，可以尝试以下方式</translation>
    </message>
    <message>
        <source>Yandex Money</source>
        <translation>Yandex Money</translation>
    </message>
    <message>
        <source>in Telegram for their help.</source>
        <translation>在电报感谢他们的帮助。</translation>
    </message>
    <message>
        <source>And to users:</source>
        <translation>致用户：</translation>
    </message>
    <message>
        <source>Using Emoji from</source>
        <translation>使来自以下的Emoji</translation>
    </message>
</context>
<context>
    <name>AccountsPage</name>
    <message>
        <source>Accounts</source>
        <translation>账户</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <source>Connecting</source>
        <translation>正在连接</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>离线</translation>
    </message>
    <message>
        <source>name: </source>
        <translation>名称：</translation>
    </message>
    <message>
        <source>url: </source>
        <translation>url:</translation>
    </message>
    <message>
        <source>status: </source>
        <translation>状态:</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <source>Disable</source>
        <translation>禁用</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>操作</translation>
    </message>
    <message>
        <source>Add account ...</source>
        <translation>添加账户……</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>启用</translation>
    </message>
    <message>
        <source>Loggining</source>
        <translation>登录中</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation>已禁用</translation>
    </message>
</context>
<context>
    <name>ChannelLabel</name>
    <message>
        <source>Public channes</source>
        <translation>公共频道</translation>
    </message>
    <message>
        <source>Private channes</source>
        <translation>私密频道</translation>
    </message>
    <message>
        <source>Direct channes</source>
        <translation>私信频道</translation>
    </message>
</context>
<context>
    <name>ChannelsPage</name>
    <message>
        <source>Options</source>
        <translation>操作</translation>
    </message>
    <message>
        <source>About</source>
        <translation>关于</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Offline</source>
        <translation>离线</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <source>Online</source>
        <translation>在线</translation>
    </message>
    <message>
        <source>Connecting</source>
        <translation>正在连接</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <source>server custom name</source>
        <translation>服务器自定义名称</translation>
    </message>
    <message>
        <source>server address</source>
        <translation>服务器地址</translation>
    </message>
    <message>
        <source>use authentication token</source>
        <translation>使用授权凭证</translation>
    </message>
    <message>
        <source>Authentication token</source>
        <translation>授权凭证</translation>
    </message>
    <message>
        <source>Username or Email address</source>
        <translation>用户名或电子邮件</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <source>Certificate options</source>
        <translation>证书选项</translation>
    </message>
    <message>
        <source>trust certificate</source>
        <translation>信任证书</translation>
    </message>
    <message>
        <source>Choose CA certificate</source>
        <translation>选择CA证书</translation>
    </message>
    <message>
        <source>CA certificate path</source>
        <translation>CA证书路径</translation>
    </message>
    <message>
        <source>Choose server certificate</source>
        <translation>选择服务器证书</translation>
    </message>
    <message>
        <source>Server certificate path</source>
        <translation>服务器证书路径</translation>
    </message>
</context>
<context>
    <name>MattermostQt</name>
    <message>
        <source>Cant open CA certificate file: &quot;%0&quot;</source>
        <translation>无法打开CA证书文件: &quot;%0&quot;</translation>
    </message>
    <message>
        <source>Cant open certificate file: &quot;%0&quot;</source>
        <translation>无法打开证书文件:&quot;%0&quot;</translation>
    </message>
    <message>
        <source>File: id(%0); </source>
        <translation>文件: id(%0);</translation>
    </message>
    <message>
        <source>Image: </source>
        <translation>图像:</translation>
    </message>
    <message>
        <source>File: </source>
        <translation>文件:</translation>
    </message>
    <message>
        <source>Empty message</source>
        <translation>消息为空</translation>
    </message>
</context>
<context>
    <name>MessageEditorBar</name>
    <message>
        <source>Message...</source>
        <translation>消息……</translation>
    </message>
    <message>
        <source>Uploading </source>
        <translation>正在上传</translation>
    </message>
    <message>
        <source>Choose image</source>
        <translation>选择图像</translation>
    </message>
    <message>
        <source>Choose document</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <source>Choose file</source>
        <translation>选择文件</translation>
    </message>
</context>
<context>
    <name>MessagesPage</name>
    <message>
        <source>get older</source>
        <translation>获取更早的消息</translation>
    </message>
    <message>
        <source>Reply</source>
        <translation>回复</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>正在删除</translation>
    </message>
</context>
<context>
    <name>MessagesPage_old</name>
    <message>
        <source>get older</source>
        <translation>获取更早的消息</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <source>Reply</source>
        <translation>回复</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <source>Choose image</source>
        <translation>选择图像</translation>
    </message>
    <message>
        <source>Choose document</source>
        <translation>选择文档</translation>
    </message>
    <message>
        <source>Choose file</source>
        <translation>选择文件</translation>
    </message>
</context>
<context>
    <name>OptionsPage</name>
    <message>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <source>Show blobs</source>
        <translation>显示 blob</translation>
    </message>
    <message>
        <source>Show blobs unders messages</source>
        <translation>在消息下方显示 blob</translation>
    </message>
    <message>
        <source>Blobs opacity value</source>
        <translation>Blob 不透明度</translation>
    </message>
    <message>
        <source>Markdown (beta)</source>
        <translation>Markdown（测试）</translation>
    </message>
    <message>
        <source>Use markdown formated text in messages</source>
        <translation>使用  markdown 文本格式消息</translation>
    </message>
    <message>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>小号</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>中号</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>大号</translation>
    </message>
    <message>
        <source>Page padding</source>
        <translation>页面填充</translation>
    </message>
    <message>
        <source>Cache size: </source>
        <translation>选择大小:</translation>
    </message>
    <message>
        <source>Clear cache</source>
        <translation>清空缓存</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Login failed because of invalid password</source>
        <translation>密码无效，登录失败</translation>
    </message>
    <message>
        <source>%0 bytes</source>
        <translation>%0 bytes</translation>
    </message>
    <message>
        <source>%0 Kb</source>
        <translation>%0 Kb</translation>
    </message>
    <message>
        <source>%0 Mb</source>
        <translation>%0 Mb</translation>
    </message>
    <message>
        <source>(you)</source>
        <translation>（你）</translation>
    </message>
    <message>
        <source>somebody</source>
        <translation>某人</translation>
    </message>
    <message>
        <source>(edited)</source>
        <translation>（已编辑）</translation>
    </message>
    <message>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <source>PB</source>
        <translation>PB</translation>
    </message>
</context>
<context>
    <name>ReplyMessageItem</name>
    <message>
        <source>Reply to</source>
        <translation>回复</translation>
    </message>
    <message>
        <source>Answer to message from</source>
        <translation>回消息给</translation>
    </message>
</context>
<context>
    <name>TeamOptions</name>
    <message>
        <source>Team Options</source>
        <translation>团队选项</translation>
    </message>
</context>
<context>
    <name>TeamsPage</name>
    <message>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <source>About</source>
        <translation>关于</translation>
    </message>
</context>
<context>
    <name>harbour-mattermost</name>
    <message>
        <source>New post on </source>
        <translation>新张贴</translation>
    </message>
    <message>
        <source> by user </source>
        <translation>通过用户</translation>
    </message>
</context>
</TS>
